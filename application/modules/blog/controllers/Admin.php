<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function blog(){
            $crud = $this->crud_function('','');
            $crud->field_type('foto','image',array('path'=>'img/blog','width'=>'499px','height'=>'310px'));
            $crud->field_type('foto_grande','image',array('path'=>'img/blog','width'=>'570px','height'=>'629px'));
            $crud->field_type('tags','tags');
            $crud->field_type('status','true_false',array('0'=>'Borrador','1'=>'Publicado'));
            $crud->field_type('idioma','dropdown',array('es'=>'Castellano','en'=>'Ingles'));
            $crud->columns("blog_categorias_id","foto","titulo","tags","fecha","idioma");
            $crud->add_action('<i class="fa fa-clipboard"></i> Clonar','',base_url('blog/admin/clonarEntrada').'/');
            $crud->field_type('user','string',$this->user->nombre);
            $crud->set_clone();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function blog_categorias(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Categoria');
            $crud->display_as('blog_categorias_nombre','Nombre');
            $crud->field_type('idioma','dropdown',array('es'=>'Castellano','en'=>'Ingles'));
            $crud = $crud->render();
            $crud->title = 'Categorias';
            $this->loadView($crud);
        }
        
        public function clonarEntrada($id){
            if(is_numeric($id)){
                $entry = new Bdsource();
                $entry->where('id',$id);
                $entry->init('blog',TRUE,'entrada');
                $data = $this->entrada;
                $entry->save($data,null,TRUE);
                header("Location:".base_url('blog/admin/blog/edit/'.$entry->getid()));
            }
        }
    }
?>
