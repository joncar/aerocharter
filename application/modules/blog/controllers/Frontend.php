<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();       
            $this->load->model('querys');     
        }
        
        function get_categorias(){
            $this->db->limit('8');
            $categorias = $this->db->get_where('blog_categorias');
            foreach($categorias->result() as $n=>$c){
                $categorias->row($n)->cantidad = $this->db->get_where('blog',array('blog_categorias_id'=>$c->id))->num_rows();
                $categorias->row($n)->link = base_url('blog').'?categorias_id='.$c->id;
            }
            return $categorias;
        }
        
        public function index(){
            $blog = new Bdsource();
            $blog->limit = array('6','0');
            $blog->order_by = array('fecha','DESC');
            if(!empty($_GET['direccion'])){
                $blog->like('titulo',$_GET['direccion']);
            }
            if(!empty($_GET['blog_categorias_id'])){
                $blog->where('blog_categorias_id',$_GET['blog_categorias_id']);
            }
            $blog->where('idioma',$_SESSION['lang']);
            if(!empty($_GET['page'])){
                $blog->limit = array(6,($_GET['page']-1));
            }else{
                $_GET['page'] = 1;
            }
            $blog->where('idioma',$_SESSION['lang']);
            $blog->init('blog');
            foreach($this->blog->result() as $n=>$b){
                $this->blog->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
                $this->blog->row($n)->foto = base_url('img/blog/'.$b->foto);
                $this->blog->row($n)->mes = strftime('%b',strtotime($b->fecha));
                $this->blog->row($n)->dia = strftime('%d',strtotime($b->fecha));
                $this->blog->row($n)->fecha = strftime('%d %b %Y',strtotime($b->fecha));
                $this->blog->row($n)->titulo = cortar_palabras(strip_tags($b->titulo),5).'...';
                $this->blog->row($n)->texto = cortar_palabras(strip_tags($b->texto),30);
                $this->blog->row($n)->categoria = $this->db->get_where('blog_categorias',array('id'=>$b->blog_categorias_id))->row()->blog_categorias_nombre;
            }
            $this->db->where('idioma',$_SESSION['lang']);
            $totalpages = round($this->db->get_where('blog')->num_rows()/6);
            $totalpages = $totalpages==0?'1':$totalpages;        
            foreach($this->blog->result() as $n=>$b){
                $this->blog->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows;                
            }
            if($this->blog->num_rows()>0){
                $this->blog->tags = $this->blog->row()->tags;
            }
            
            $recientes = new Bdsource();
            $recientes->limit = array(4);                         
            $recientes->order_by = array('id','desc');
            $recientes->where('idioma',$_SESSION['lang']);
            $recientes->init('blog',FALSE,'recientes');
            foreach($this->recientes->result() as $n=>$b){
                $this->recientes->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
                $this->recientes->row($n)->foto = base_url('img/blog/'.$b->foto);
                $this->recientes->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();                
                $this->recientes->row($n)->texto = substr($b->texto,0,80);
                $this->recientes->row($n)->fecha = strftime('%d %b %Y',strtotime($b->fecha));
            }
            $tags = '';
            if($this->blog->num_rows()>0){
                foreach(explode(',',$this->blog->row(0)->tags) as $t){
                    $tags = '<a href="#">'.$t.'</a>';
                }
            }


            $this->loadView(
                    array(
                        'view'=>'frontend/main',
                        'detail'=>$this->blog,
                        'total_pages'=>$totalpages,
                        'current_page'=>$_GET['page'],
                        'title'=>'Blog',
                        'tags'=>$tags,
                        'categorias'=>$this->get_categorias(),
                        'recientes'=>$this->recientes,
                        'link'=>'blog'
                    ));
        }
        
        public function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $blog = new Bdsource();
                $blog->where('id',$id);
                $blog->init('blog',TRUE);
                $this->blog->link = site_url('blog/'.toURL($this->blog->id.'-'.$this->blog->titulo));
                $this->blog->foto = base_url('img/blog/'.$this->blog->foto);
                $this->blog->mes = strftime('%b',strtotime($this->blog->fecha));
                $this->blog->dia = strftime('%d',strtotime($this->blog->fecha));
                $this->blog->fecha = strftime('%d %b %Y',strtotime($this->blog->fecha));
                $this->blog->categoria = $this->db->get_where('blog_categorias',array('id'=>$this->blog->blog_categorias_id))->row()->blog_categorias_nombre;
                if($blog->num_rows()>0){
                    $blog->vistas++;
                    $blog->save();
                }
                $comentarios = new Bdsource();
                $comentarios->where('blog_id',$this->blog->id);
                $comentarios->init('comentarios');
                $relacionados = new Bdsource();
                $relacionados->limit = array(4); 
                $relacionados->where('blog_categorias_id',$this->blog->blog_categorias_id);
                $relacionados->where('id !=',$this->blog->id);
                $relacionados->where('idioma',$_SESSION['lang']);
                $relacionados->order_by = array('id','desc');
                $relacionados->init('blog',FALSE,'relacionados');
                foreach($this->relacionados->result() as $n=>$b){
                    $this->relacionados->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
                    $this->relacionados->row($n)->foto = base_url('img/blog/'.$b->foto);
                    $this->relacionados->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();
                    $this->relacionados->row($n)->categoria = $this->db->get_where('blog_categorias',array('id'=>$b->blog_categorias_id))->row()->blog_categorias_nombre;                
                }

                $prev = '';
                $next = '';
                $blogs = $this->db->get_where('blog',array('status'=>1,'idioma'=>$_SESSION['lang']));
                $entradas = array();
                foreach($blogs->result() as $n=>$b){
                    $entradas[] = $b;
                }
                foreach($entradas as $n=>$b){
                    if(!empty($entradas[$n-1])){
                        $bb = $entradas[$n-1];
                        $prev = '<a href="'.site_url('blog/'.toUrl($bb->id.'-'.$bb->titulo)).'" class="blog-item-more left"><i class="fa fa-angle-left"></i>&nbsp;Anterior post</a>';
                    }

                    if(!empty($entradas[$n+1])){
                        $bb = $entradas[$n+1];
                        $next = '<a href="'.site_url('blog/'.toUrl($bb->id.'-'.$bb->titulo)).'" class="blog-item-more right">Siguiente post &nbsp;<i class="fa fa-angle-right"></i></a>';
                    }
                }
                $this->loadView(
                    array(
                        'view'=>'frontend/detail',
                        'detail'=>$this->blog,
                        'title'=>$this->blog->titulo,
                        'comentarios'=>$this->comentarios,
                        'categorias'=>$this->get_categorias(),
                        'relacionados'=>$this->relacionados,
                        'prev'=>$prev,
                        'next'=>$next,
                        'link'=>'blog'
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
        
        public function comentarios(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('autor','Autor','required')
                                  ->set_rules('texto','Comentario','required')
                                  ->set_rules('blog_id','','required|numeric');
            if($this->form_validation->run()){
                $data = array();
                foreach($_POST as $n=>$p){
                    $data[$n] = $p;
                }
                $data['fecha'] = date("Y-m-d");
                $this->db->insert('comentarios',$data);
                $_SESSION['mensaje'] = $this->success('Comentario añadido con éxito <script>document.reload();</script>');
                header("Location:".base_url('blog/frontend/read/'.$_POST['blog_id']));
            }else{
                $_SESSION['mensaje'] = $this->error('Comentario no enviado con éxito');
                header("Location:".base_url('blog/frontend/read/'.$_POST['blog_id']));                
            }
        }

        public function eventos(){
            $this->loadView(array(
                'view'=>'eventos'
            ));
        }
    }
?>
