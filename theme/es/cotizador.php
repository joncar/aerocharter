<!DOCTYPE html>
<html lang="es-mx">
    <head>
        <?php include('header.php');?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

    <body>
        <h1>Aerocharter</h1>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php include('menu-interno.php');?>
        </nav>

        <div class="header2">
            <h2>Cotiza ahora</h2>
        </div>

        <div class="process">
            <div class="card1">
                <h2>Cotiza</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis augue sollicitudin facilisis sollicitudin. Phasellus eleifend convallis ligula.</p>
            </div>
            <div class="card2">
                <h2>Solicita</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis augue sollicitudin facilisis sollicitudin. Phasellus eleifend convallis ligula.</p>
            </div>
            <div class="card3">
                <h2>Emplea</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis augue sollicitudin facilisis sollicitudin. Phasellus eleifend convallis ligula.</p>
            </div>
        </div>

        <div class="cotizador" style="margin-bottom: 100px;">
            <h2>Tu Información</h2>
            <form class="form-contacto form-horizontal" method="post" action="cotizador-envio.php">
                <div>
                    <div class="input">
                        <span style="color: #5ecbea">1)</span>
                        <h3>Nombre de Aerolínea</h3>
                        <input type="text" name="nombreaerolinea" id="nombreaerolinea" placeholder="...">
                    </div>

                    <div class="input">
                        <span style="color: #5ecbea">2)</span>
                        <h3>Aeropuertos en donde opera</h3>
                        <input type="text" name="nombreoperacion" id="nombreoperacion" placeholder="...">
                    </div>

                    <div class="input">
                        <span style="color: #5ecbea">3)</span>
                        <h3>Numero de operaciones mensuales aproximado</h3>
                        <input type="text" name="operacionesmensuales" id="operacionesmensuales" placeholder="...">
                    </div>

                    <div class="input input-cotiza">
                        <span style="color: #5ecbea">4)</span>
                        <h3>Servicios</h3>
                        <fieldset>

                            <div class="checkbox" style="margin-bottom: 20px;" id="width-check">
                              <label><input type="checkbox" name="numero[]" value="1" style="position: inherit; width: 15%;">Arrancador neumático<br>(Air Start Unit)</label>
                            </div>
                            <div class="checkbox" style="margin-bottom: 20px;" id="width-check">
                              <label><input type="checkbox" name="numero[]" value="2" style="position: inherit;  width: 15%;">Planta Eléctrica<br>(Ground Power Unit)</label>
                            </div>
                            <div class="checkbox" style="margin-bottom: 20px;" id="width-check">
                              <label><input type="checkbox" name="numero[]" value="3" style="position: inherit;  width: 15%;">Loader<br>(Cargo Loader)</label>
                            </div>

                            <div class="checkbox" style="margin-bottom: 20px;" id="width-check">
                              <label><input type="checkbox" name="numero[]" value="4" style="position: inherit;  width: 15%;">Banda Conveyor<br>(Belt Loader)</label>
                            </div>
                            <div class="checkbox" style="margin-bottom: 20px;" id="width-check">
                              <label><input type="checkbox" name="numero[]" value="5" style="position: inherit;  width: 15%;">Escaleras<br>(Boarding Stairs)</label>
                            </div>
                            <div class="checkbox" style="margin-bottom: 20px;" id="width-check">
                              <label><input type="checkbox" name="numero[]" value="6" style="position: inherit;  width: 15%;">Tractor para Pushback<br>(Pushback Tractor)</label>
                            </div>
                            <div class="checkbox" style="margin-bottom: 20px;" id="width-check">
                              <label><input type="checkbox" name="numero[]" value="7" style="position: inherit;  width: 15%;">Tractor equipajero<br>(Baggage Tractors)</label>
                            </div>
                            <div class="checkbox" style="margin-bottom: 20px;" id="width-check">
                              <label><input type="checkbox" name="numero[]" value="8" style="position: inherit;  width: 15%;">Carro de Aguas Negras<br>(Lavatory Service Truck)</label>
                            </div>
                            <div class="checkbox" style="margin-bottom: 20px;" id="width-check">
                              <label><input type="checkbox" name="numero[]" value="9" style="position: inherit;  width: 15%;">Carro de aguas potables<br>(Potable Water Truck)</label>
                            </div>
                        </fieldset>
                    </div>

                </div>


                <div>
                    <!--
                    <div class="input input-cotiza">
                        <h3>Tipo de Aerolínea</h3>
                        <fieldset>
                            <label>
                              <input type="radio" name="test" value="small" checked>
                              <img src="[base_url]images/select-01.png"> a.Pasajero
                            </label>

                            <label>
                              <input type="radio" name="test" value="big">
                              <img src="[base_url]images/select-02.png"> b.Carga
                            </label>
                        </fieldset>
                    </div>

                    <div class="input input-cotiza">
                        <h3>Tipos de Vuelos</h3>
                        <fieldset>
                            <label>
                              <input type="radio" name="test" value="small">
                              <img src="[base_url]images/select-01.png"> a.Vivo/Vivo
                            </label>

                            <label>
                              <input type="radio" name="test" value="big">
                              <img src="[base_url]images/select-02.png">  b. Vacío/Vivo
                            </label>

                            <label>
                              <input type="radio" name="test" value="big">
                              <img src="[base_url]images/select-02.png">  c. Vivo/Vacío
                            </label>
                        </fieldset>
                    </div>-->
                    

                    <div class="input input-cotiza">
                        <span style="color: #5ecbea">5)</span>
                        <h3>Tipo de Aerolínea</h3><br>
                        <select class="form-control" id="selectaerolinea" name="selectaerolinea">
                          <option disabled="seleccionar">Select</option>
                          <option value="Pasajero">a. Pasajero</option>
                          <option value="Carga">b. Carga</option>
                        </select>
                    </div>

                    <div class="input input-cotiza">
                        <span style="color: #5ecbea">6)</span>
                        <h3>Tipo de Vuelo</h3><br>
                        <select class="form-control" id="selectvuelo" name="selectvuelo">
                          <option disabled="seleccionar">Select</option>
                          <option value="Vivo/Vivo">a. Vivo/Vivo</option>
                          <option value="Vacío/Vivo">b. Vacío/Vivo</option>
                          <option value="Vivo/Vacío">c. Vivo/Vacío</option>
                        </select>
                    </div>

                    <div class="input input2">
                        <span style="color: #5ecbea">7)</span>
                        <h3>Datos de Contacto</h3>
                        <input type="text" name="nombre" id="nombre" placeholder="Nombre...">
                        <input type="text" name="puesto" id="puesto" placeholder="Posición en la compañia...">
                        <input type="text" name="ciudad" id="ciudad" placeholder="Ciudad de ubicación...">
                        <input type="text" name="telefono" id="telefono" placeholder="Teléfono...">
                        <input type="email" name="correo" id="correo" placeholder="Correo...">
                    </div>


                </div>
                <input type="submit" value="Enviar">
            </form>
        </div>

        <div class="bg-black container-fluid contenedor-mapa-sitio">
            <?php include('mapa-sitio.php');?>
        </div>

        <?php include('librerias.php');?>

    </body>
</html>
