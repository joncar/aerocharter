<!DOCTYPE html>
<html lang="es-mx">
    <head>
        <?php include('header.php');?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

    <body>
        <h1>Aerocharter</h1>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php include('menu-interno.php');?>
        </nav>

        <div class="envio" id="envio-mensaje">
            <div class="container text-center">
              <h1>Gracias!  <?php echo $nombre ?>!</h1>
              <h2>Tu mensaje ha sido enviado.</h2>
            </div>
        </div>

        <?php
        	$nombre 	= htmlspecialchars ($_POST["nombre"]);
          $correo 	= htmlspecialchars ($_POST["correo"]);
          $mensaje 	= htmlspecialchars ($_POST["mensaje"]);

          $recipients = array(
          "alan@bluepixel.mx",
          // more emails
        );
          $to     = implode(',', $recipients);
          $subject        = "Nueva cotización - Aerocharter";
          $headers        = "MIME-Version: 1.0" . "\r\n";
          $headers       .= "Content-type:text/html;charset=UTF-8" . "\r\b";
          $headers       .= "From: aerocharter.com";

          $message       .= "<p style='text-align:left;margin-bottom:30px'>";
          $message       .= "<img src='http://demoweb.aerocharter.com.mx/web/images/aerocharter-logo.png' style='width:160px; margin-bottom:30px'/><br>";
          $message       .= "<p>";

          $message       .= "<strong> Nombre:   </strong>"  . $nombre   . "<br>";
          $message       .= "<strong> Correo: </strong> " . $correo . "<br>" ;
          $message       .= "<strong> Mensaje: </strong> " . $mensaje . "<br>" ;

          $message       .= $mensaje;
          $message       .= "</p>";

          mail($to,$subject,$message,$headers);


        ?>

        <div class="bg-black container-fluid contenedor-mapa-sitio">
            <?php include('mapa-sitio.php');?>
        </div>

        <?php include('librerias.php');?>

    </body>

</html>
