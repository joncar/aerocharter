<a class="navbar-brand" href="index.php" id="logo-menu">
    <img src="[base_url]images/aerocharter-logo.png" alt="Logo de Aerocharter">
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse flex-row-reverse" id="navbarNav">
    <ul class="navbar-nav">
        <li class="nav-item active">
            <a class="nav-link" href="<?= base_url() ?>index.php#home">Inicio <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url() ?>index.php#nosotros">Nosotros</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url() ?>index.php#servicios">Servicios</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="<?= base_url() ?>blog">Blog</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="<?= base_url() ?>index.php#historia">Historia</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="<?= base_url() ?>index.php#contacto">Contacto</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled btn-cotiza" href="<?= base_url() ?>cotizador.php" style="border-radius: 10px;">Cotizar ahora</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="<?= base_url() ?>main/traduccion/es">Español</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="<?= base_url() ?>main/traduccion/en">Inglés</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="#"><i class="fab fa-facebook"></i></a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="#"><i class="fab fa-twitter"></i></a>
        </li>
    </ul>
</div>
