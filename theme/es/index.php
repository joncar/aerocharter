<!DOCTYPE html>
<html lang="es-mx">
    <head>
        <?php include('header.php');?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

    <body>
        <h1>Aerocharter</h1>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php include('menu.php');?>
        </nav>

        <div id="home">
            <h3>Servicios integrales en tierra</h3>
            <h2>Para la industria aérea</h2>
            <p>45 años de experiencia ofreciendo servicios técnicos, administrativos, operacionales y comerciales a la industria aérea a nivel global.</p>
            <figure data-aos="fade-up" data-aos-duration="900000">
                <a href="cotizador.php"><img src="[base_url]images/circle_of_service.png" alt="Círculo de Servicios Aerocharter" class="slide-fwd-center"></a>
            </figure>
        </div>

        <div id="nosotros">
            <div class="white">
                <h4>LÍDER DE TALLA GLOBAL</h4>
                <p>Aerocharter, una empresa 100% mexicana que ofrece desde hace 45 años un amplio portafolio de servicios administrativos, operacionales, técnicos y comerciales a la industria aérea a nivel global, posicionándonos como la mejor opción para nuestros clientes, buscando que obtengan cada día atención de alta calidad y un trato personalizado.</p>
                <!--<a href="#">Leer más</a>-->
            </div>
            <div class="blue">
                <h4 data-aos="fade-in" data-aos-duration="8000">MISIÓN</h4>
                <p>Proveer soporte operacional, técnico y administrativo a la industria aérea a través de servicios que agregan valor y satisfacen las expectativas de nuestros clientes, empleados, accionistas y usuarios de nuestros servicios.</p>
            </div>
            <div class="blue">
                <h4 data-aos="fade-in" data-aos-duration="8000">VISIÓN</h4>
                <p>Ser reconocido por la industria aérea como la mejor organización proveedora de servicios operacionales, técnicos, administrativos y comerciales con un nivel de clase mundial.</p>
            </div>
        </div>

        <div id="servicios">
            <h2>SERVICIOS</h2>

            <!--
            <ul class="nav nav-tabs">
                <li class="active tab1">Servicio<br>de carga</a></li>
                <li class="tab2"><a data-toggle="tab" href="#menu1">Rep.<br>Legal</a></li>
                <li class="tab3"><a data-toggle="tab" href="#menu2">Servicio<br> a pax.</a></li>
                <li class="tab4"><a data-toggle="tab" href="#menu3">Operación<br> en rampa</a></li>
                <li class="tab5"><a data-toggle="tab" href="#menu4">Venta<br> de GSA</a></li>
            </ul>-->

            <!--
            <div class="tab-content">
                <div id="casa" class="tab-pane active">
                    <h3>HOME</h3>
                    <ul>
                        <li>Agente General de Ventas (GSSA)<br>Capacitado, con experiencia y reconocido.</li>
                        <li>Servicio a Vuelos de Fletamento y Carga</li>
                        <li>Manejo y almacenaje de Carga</li>
                        <li>Supervisión del manejo de carga</li>
                        <li>Manejo de carga en plataforma y almacén</li>
                        <li>Desarrollo de negocios comerciales de carga</li>
                        <li>Servicio al Cliente bilingüe</li>
                        <li>Ventas / Ventas a Consignación</li>
                    </ul>
                </div>
                <div id="menu1" class="tab-pane fade">
                    <h3>Menu 1</h3>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <h3>Menu 2</h3>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                </div>
                <div id="menu3" class="tab-pane fade">
                    <h3>Menu 3</h3>
                    <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                </div>
                <div id="menu4" class="tab-pane fade">
                    <h3>Menu 4</h3>
                    <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                </div>
            </div>-->


            <div>
              <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#1" class="btn-tab-1 montserrat"><i>SERVICIO<br>DE CARGA</i></a>
                </li>

                <li>
                    <a data-toggle="tab" href="#2" class="btn-tab-2 montserrat hidden-xs"><i>REPRESENTACIÓN<br>LEGAL</i></a>
                    <a data-toggle="tab" href="#2" class="btn-tab-2 montserrat visible-xs"><i>REP.<br>LEGAL</i></a>
                </li>

                <li>
                    <a data-toggle="tab" href="#3" class="btn-tab-3 montserrat"><i>SERVICIO<br>A PAX</i></a>
                </li>

                <li>
                    <a data-toggle="tab" href="#4" class="btn-tab-4 montserrat"><i>OPERACIÓN<br>EN RAMPA</i></a>
                </li>

                <li>
                    <a data-toggle="tab" href="#5" class="btn-tab-5 montserrat"><i>VENTA<br>DE GSA</i></a>
                </li>
              </ul>

              <div class="tab-content">
                <div id="1" class="row tab-pane fade in active">
                  <div class="col-xs-12 col-sm-12">
                      <div class="col-xs-12 col-sm-12 text-center titulo-tabs montserrat">
                        <b>SERVICIO DE CARGA</b>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-6">
                          <img src="[base_url]images/tab1.png" class="center-block" alt="Servicios">
                      </div>

                      <div class="col-xs-12 col-sm-12 col-md-6">
                          <ul style=" list-style-type: none;">
                            <li><i class="fas fa-plane"></i> Agente General de Ventas (GSSA) Capacitado, con experiencia y reconocido. ​</li>
                            <li><i class="fas fa-plane"></i> Servicio a Vuelos de Fletamento y Carga</li>
                            <li><i class="fas fa-plane"></i> Manejo y almacenaje de Carga</li>
                            <li><i class="fas fa-plane"></i> Supervisión del manejo de carga</li>
                            <li><i class="fas fa-plane"></i> Manejo de carga en plataforma y almacén </li>
                            <li><i class="fas fa-plane"></i> Desarrollo de negocios comerciales de carga</li>
                            <li><i class="fas fa-plane"></i> Servicio al Cliente bilingüe</li>
                            <li><i class="fas fa-plane"></i> Ventas / Ventas a Consignación</li>
                          </ul>
                      </div>
                  </div>
                </div>

                <div id="2" class="row tab-pane fade">
                  <div class="col-sm-12">
                      <div class="col-sm-12 text-center titulo-tabs montserrat">
                        <b>REPRESENTACIÓN LEGAL</b>
                      </div>
                      <div class="col-sm-6">
                          <img src="[base_url]images/tab2.png" class="center-block" alt="Servicios">
                      </div>

                      <div class="col-sm-6">
                          Representante legal ante autoridades gubernamentales y necesidades comerciales para la obtención de: 
                          <ul style=" list-style-type: none;">
                            <li><i class="fas fa-plane"></i> Certificado de Operador Aéreo (AOC).</li>
                            <li><i class="fas fa-plane"></i> Permisos para operaciones de fletamento y/o regulares. </li>
                            <li><i class="fas fa-plane"></i> Gestión de Slots.</li>
                            <li><i class="fas fa-plane"></i> Acercamiento con autoridades, Contratación de personal.</li>
                            <li><i class="fas fa-plane"></i> Atención especial a vuelos gubernamentales</li>
                            <li><i class="fas fa-plane"></i> Asuntos gubernamentales</li>
                          </ul>
                      </div>
                  </div>
                </div>

                <div id="3" class="row tab-pane fade">
                  <div class="col-sm-12">
                      <div class="col-sm-12 text-center titulo-tabs montserrat">
                        <b>SERVICIO A PAX</b>
                      </div>
                      <div class="col-sm-6">
                          <img src="[base_url]images/tab3.png" class="center-block" alt="Servicios">
                      </div>

                      <div class="col-sm-6">
                          <ul style=" list-style-type: none;">
                            <li><i class="fas fa-plane"></i> Venta y reservaciones</li>
                            <li><i class="fas fa-plane"></i> Manejo de Equipaje </li>
                            <li><i class="fas fa-plane"></i> Atención en Mostradores </li>
                            <li><i class="fas fa-plane"></i> Servicio a Clientes</li>
                            <li><i class="fas fa-plane"></i> Asistencia de silla de ruedas WCH</li>
                            <li><i class="fas fa-plane"></i> Sala de Equipaje </li>
                            <li><i class="fas fa-plane"></i> Manejo de Equipaje en transferencia/conexión</li>
                          </ul>
                      </div>
                  </div>
                </div>

                <div id="4" class="row tab-pane fade">
                  <div class="col-sm-12">
                      <div class="col-sm-12 text-center titulo-tabs montserrat">
                        <b>OPERACIÓN EN RAMPA</b>
                      </div>
                      <div class="col-sm-6">
                          <img src="[base_url]images/tab4.png" class="center-block" alt="Servicios">
                      </div>

                      <div class="col-sm-6">
                          <ul style=" list-style-type: none;">
                            <li><i class="fas fa-plane"></i> Peso y Balance (despacho)</li>
                            <li><i class="fas fa-plane"></i> Carga y descarga</li>
                            <li><i class="fas fa-plane"></i> Limpieza de Cabina / Limpieza Profunda</li>
                            <li><i class="fas fa-plane"></i> Movimiento de aeronaves (pushback y remolque)</li>
                            <li><i class="fas fa-plane"></i> Señalamiento </li>
                            <li><i class="fas fa-plane"></i> Aseguramiento y seguridad de la Carga</li>
                            <li><i class="fas fa-plane"></i> Servicio de agua potable y de sanitarios</li>
                            <li><i class="fas fa-plane"></i> Plantas eléctricas (GPU) y arrancadores (ASU)</li>
                            <li><i class="fas fa-plane"></i> Mantenimiento Línea (En proceso)</li>
                          </ul>
                      </div>
                  </div>
                </div>

                <div id="5" class="row tab-pane fade">
                  <div class="col-sm-12">
                      <div class="col-sm-12 text-center titulo-tabs montserrat">
                        <b>VENTA DE GSA</b>
                      </div>
                      <div class="col-sm-6">
                          <img src="[base_url]images/tab5.png" class="center-block" alt="Servicios">
                      </div>

                      <div class="col-sm-6">
                          <ul style=" list-style-type: none;">
                            <li><i class="fas fa-plane"></i> Equipos de ventas entrenados, experimentados y confiables</li>
                            <li><i class="fas fa-plane"></i> Relaciones con cliente establecidas por muchos años</li>
                            <li><i class="fas fa-plane"></i> Conocimiento del mercado local y global</li>
                            <li><i class="fas fa-plane"></i> Desarrollo de estrategias de mercadeo, campañas de publicidad y posicionamiento</li>
                            <li><i class="fas fa-plane"></i> Contabilidad y cobranza por medio del CASS</li>
                            <li><i class="fas fa-plane"></i> Profundo conocimiento de los diferentes tipos de aeronaves y de todos los tipos de cargas aceptables según las regulaciones del transporte aéreo.</li>
                            <li><i class="fas fa-plane"></i> Regulación de Mercancías Peligrosas.</li>
                            <li><i class="fas fa-plane"></i> Diseño, desarrollo y creación de nuevos productos y soluciones logísticas</li>
                            <li><i class="fas fa-plane"></i> Reportes de Ventas y Estadística.</li>
                            <li><i class="fas fa-plane"></i> Análisis de Mercado y estudios de desarrollo</li>
                          </ul>
                      </div>
                  </div>
                </div>

              </div>
            </div>


        </div>

        <div id="noticias">
            <h2>Blog</h2>
            <div class="noticias">
                <div class="not1">
                </div>
                <div class="not2">
                    <figure>
                        <img src="[base_url]images/not2.png" alt="Noticia 2">
                    </figure>
                    <div>
                        <h4>Engineers are weird</h4>
                        <p data-aos="fade-up" data-aos-duration="5000">The flight from Apodaca to Mumbai was chartered by DHL Global Forwarding, with the shipment packaged in a 7-metre long box alongside some support equipment required for loading.</p>
                        <a href="entrada.html">Leer más</a>
                    </div>
                </div>
                <div class="not1">
                </div>
            </div>
        </div>

        <div id="historia">
            <h2>Historia</h2>
            <div class="historia">
                <h3>Historia</h3>

                <!--
                <ul>
                    <li class="li1"><b>1973</b><br>Aerocharter fue fundada en 1973 con el propósito de proveer servicios estratégicos a las aerolíneas internacionales que volaban a México. En 1992 la compañía se convirtió en la mas grande de su tipo.</li>
                    <li class="li2"><b>19XX</b><br>Su división de GSSAs representa a más de 10 operadores de carga internacionales.</li>
                    <li class="li3"><b>19XX</b><br>Actualmente Aerocharterestá posicionada como un proveedor de servicios premium, con el objetivo de fortalecer las relaciones de negocios con sus clientes, autoridades y proveedores de servicio a través de liderazgo, trabajo en equipo y un staff experimentado.</li>
                    <li class="li4"><b>19XX</b><br>Con un servicio One-Stop Shop, Aerocharter satisface las necesidades de servicio de sus clientes, proporcionando un servicio personalizado con servicio de alta calidad en el despacho, operaciones terrestres y asuntos legales con la DGAC y Aeropuertos.</li>
                </ul>-->

                <p class="li1 text-center text-history">
                  <b>Aerocharter fue fundada en 1973 con el propósito de proveer servicios estratégicos a las aerolíneas internacionales que volaban a México. En 1992 la compañía se convirtió en la más grande de su tipo.<br>
                  Su división de GSSAs representa a más de 10 operadores de carga internacionales.<br>
                  Actualmente Aerocharter está posicionada como un proveedor de servicios Premium, con el objetivo de fortalecer las relaciones de negocios con sus clientes, autoridades y proveedores de servicio a través de liderazgo, trabajo en equipo y un staff experimentado.<br>
                  Con un servicio one-stop shop, Aerocharter satisface las necesidades de servicio de sus clientes, proporcionando un servicio personalizado con servicio de alta calidad en el despacho, operaciones terrestres y asuntos legales con la DGAC y Aeropuertos.
                </p>
            </div>

            <div class="clientes">
                <!--
                <div class="excelencia-texto">
                  <p>"La excelencia es el resultado de personas íntegras trabajando con pasión y seguridad como parte de un equipo”</p>
                </div>-->


                <div class="excelencia-texto">
                  <p>Nuestros clientes</p>
                </div>

                <div>
                    <!--<a href="https://www.alternativeairlines.com/taca-airlines" target="blank"><img src="[base_url]images/tacaperu-logo.png" alt="Logo de Taca Peru"></a>-->
                    <a href="https://www.interjet.com" target="blank"><img src="[base_url]images/interjet-logo.png" alt="Logo de Interjet"></a>
                    <!--<a href="https://www.klm.com/home/mx/en?popup=no&WT.mc_id=c_mx_sea_google_brand_search_null_null&gclid=EAIaIQobChMI2fbFrsf33gIViTxpCh3buQVYEAAYASAAEgJsXfD_BwE&gclsrc=aw.ds" target="blank"><img src="[base_url]images/klm-logo.png" alt="Logo de KLM"></a>-->
                    <a href="http://demoweb.aerocharter.com.mx/web/index.html" target="blank"><img src="[base_url]images/bora-logo.png" alt="Logo de Volga-Dnepr Airlines"></a>
                    <!--<a href="https://www.alternativeairlines.com/taca-airlines" target="blank"><img src="[base_url]images/taca-logo.png" alt="Logo de Taca"></a>-->
                    <!--<a href="https://www.iccs.com.mx/" target="blank"><img src="[base_url]images/iccs-logo.png" alt="Logo de ICCS Mexico and Latin America"></a>-->
                    <!--<a href="https://www.avianca.com/mx/es/?gclid=EAIaIQobChMI9_CN_Mf33gIVAgxpCh0XFgP9EAAYASAAEgJTKfD_BwE&gclsrc=aw.ds" target="blank"><img src="[base_url]images/aviancacargo-logo.png" alt="Logo de Avianca Cargo"></a>-->
                    <!--<a href="https://www.airbridgecargo.com/en" target="blank"><img src="[base_url]images/airbridgecargo-logo.png" alt="Logo de Air Bridge Cargo"></a>-->
                    <a href="http://www.polaraircargo.com/" target="blank"><img src="[base_url]images/polarair-logo.png" alt="Logo de Polar Air Cargo"></a>
                    <a href="https://www.avianca.com/mx/es/?gclid=EAIaIQobChMIvKSfjsv33gIVCzxpCh2MCQJfEAAYASAAEgL0vfD_BwE&gclsrc=aw.ds" target="blank"><img src="[base_url]images/avianca-logo.png" alt="Logo de Avianca"></a>
                    <a href="https://flairair.ca/" target="blank"><img src="[base_url]images/flair-logo.png" alt="Logo de Flair Airlines"></a>
                    <a href="http://www.mexicana.com/" target="blank"><img src="[base_url]images/mexicana-logo.png" alt="Logo de Mexicana"></a>

                    <a href="http://www.dhl.com/en.html" target="blank"><img src="[base_url]images/dhl-logo.png" alt="Logo DHL"></a>
                    <a href="http://www.aerounion.com.mx/" target="blank"><img src="[base_url]images/aerounion-logo.png" alt="Logo Aero Union"></a>
                    <a href="https://www.ups.com/us/en/global.page" target="blank"><img src="[base_url]images/ups-logo.png" alt="Logo UPS"></a>
                    <a href="https://www.hainanairlines.com/MX/GB/Home?gclid=EAIaIQobChMIlu7em8733gIVgbjACh0DsAZNEAAYASAAEgJgDvD_BwE" target="blank"><img src="[base_url]images/hainan-logo.png" alt="Logo Hainan"></a>
                    <a href="https://www.nationaljets.com/" target="blank"><img src="[base_url]images/national-logo.png" alt="Logo National Jets"></a>
                    <a href="https://www.china-airlines.com/us/en" target="blank"><img src="[base_url]images/china-logo.png" alt="Logo China Airlines"></a>
                    <a href="https://www.estafeta.com/" target="blank"><img src="[base_url]images/estafeta-logo.png" alt="Logo Estafeta"></a>
                    <a href="https://www.cargologicair.com/" target="blank"><img src="[base_url]images/cargo-logic-logo.png" alt="Logo Cargo Logic Air"></a>
                    <a href="http://www.aeronavestsm.com/" target="blank"><img src="[base_url]images/tsm-logo.png" alt="Logo Cargo Logic Air"></a>
                    <a href="https://global.csair.com/" target="blank"><img src="[base_url]images/china-southern-logo.png" alt="Logo China Southern"></a>
                    <a href="http://www.atlasair.com/holdings/index.asp" target="blank"><img src="[base_url]images/atlas-logo.png" alt="Logo Atlas Air"></a>
                    <a href="http://www.aviancacargo.com/index.aspx" target="blank"><img src="[base_url]images/tampa-logo.png" alt="Logo Tampa cargo"></a>
                    <a href="http://www.atlanta.is/" target="blank"><img src="[base_url]images/atlanta-logo.png" alt="Logo Atlanta"></a>
                    <a href="https://www.afklcargo.com/WW/en/local/homepage/homepage.jsp" target="blank"><img src="[base_url]images/airfrance-logo.png" alt="Logo Air France Cargo"></a>
                    <a href="https://www.volaris.com/?gclid=EAIaIQobChMIn52F8dP33gIVgh5pCh1NBQU9EAAYASAAEgJczfD_BwE" target="blank"><img src="[base_url]images/volaris-logo.png" alt="Logo Volaris"></a>
                    <a href="http://www.kalittaair.com/" target="blank"><img src="[base_url]images/kalitta-logo.png" alt="Logo Kalitta Air"></a>
                </div>
            </div>

            <div class="lista">
                <h3>45 AÑOS DE EXPERIENCIA</h3>
                <ul>
                    <li><i class="fas fa-plane"></i> Fundadores de la primera aerolínea 100% mexicana dedicada a la carga doméstica e internacional y operando un modelo de bajo costo con un desempeño de 98.9% de OTP (OnTime Performance).</li>
                    <li><i class="fas fa-plane"></i> Ampliamente reconocidos en la industria aérea nacional e internacional.</li>
                    <li><i class="fas fa-plane"></i> Pioneros en el servicio tipo charter nacional e internacional.</li>
                    <li><i class="fas fa-plane"></i> Fortaleza financiera para emprender nuevos negocios.</li>
                    <li><i class="fas fa-plane"></i> Alto nivel de relaciones de industria y gubernamentales.</li>
                    <li><i class="fas fa-plane"></i> Equipo de alta dirección altamente calificado en la industria.</li>
                </ul>
                <figure>
                    <img src="[base_url]images/cockpit.png" alt="Aerocharter cockpit">
                </figure>
            </div>
        </div>

        <div id="equipo" class="hidden-xs">
            <h2>Equipo</h2>
            <div>
                <div id="uno" data-aos="fade-up" data-aos-duration="9000">
                    <img src="[base_url]images/vector-avion/Ilustracion-fondo.png" alt="Equipo de Aerocharter">
                </div>

                <div id="dos" data-aos="slide-right" data-aos-duration="3000" data-aos-easing="ease-in-quad">
                    <img src="[base_url]images/vector-avion/Ilustracion-01.png" alt="Equipo de Aerocharter">
                </div>

                <div id="tres" data-aos="fade-up" data-aos-duration="3000" data-aos-easing="ease-in-quad">
                    <img src="[base_url]images/vector-avion/Ilustracion-02.png" alt="Equipo de Aerocharter">
                </div>
            </div>
        </div>

        <!-- movil -->
        <div class="visible-xs">
            <div data-aos="fade-up" data-aos-duration="9000">
                <img src="[base_url]images/vector-avion/Ilustracion-01-movil.png" alt="Equipo de Aerocharter">
            </div>
        </div>

        <div id="contacto">
            <?php include('footer.php');?>
        </div>

        <div class="bg-black container-fluid contenedor-mapa-sitio">
            <?php include('mapa-sitio.php');?>
        </div>

        <?php include('librerias.php');?>


    </body>
</html>
