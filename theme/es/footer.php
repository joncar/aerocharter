<div>
    <h2>Contáctanos</h2>
    <a href="mailto:contacto@aerocharter.mx"><i class="fa fa-envelope"></i> contacto@aerocharter.mx</a>
    <a href="tel:+52155933040"><i class="fa fa-phone"></i> 1(+52)55933040</a>
    <i class="fas fa-map-marker-alt"></i> Av. Paseo de las Palmas #215, Oficina 503 Col. Lomas de Chapultepec V Sección, Del. Miguel Hidalgo, C.P. 11000, CDMX.
</div>
<div class="blue-bg text-center">
    <figure>
        <h4>Sobre Nosotros</h4>
        <p>
            45 años ofreciendo servicios técnicos, administrativos, operacionales y comerciales a la industria aérea a nivel global.
            <a href="<?= base_url() ?>cotizador.php"><button class="btn btn-footer"><b>¡Cotizar ahora!</b></button></a>
        </p>
    </figure>
</div>
<div class="formulario">
    <form class="form-contacto form-horizontal" method="post" action="contacto-envio.php">
        <input type="text" name="nombre" id="nombre" placeholder="Nombre..." required>
        <input type="email" name="correo" id="correo" placeholder="Correo..." required>
        <textarea rows="4" required placeholder="Mensaje..."></textarea>
        <input type="submit" value="Enviar">
    </form>
</div>
