<!DOCTYPE html>
<html lang="es-mx">
    <head>
        <?php include('header.php');?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

    <body>
        <h1>Aerocharter</h1>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php include('menu-interno.php');?>
        </nav>

        <div class="header">
            <h2>News</h2>
        </div>

        <div class="entradas">
            
            <?php 
                $colores = array('green','red','blue2','yellow','pink');
                $x = 0;
                foreach($detail->result() as $d): 
            ?>
                <a href="<?= $d->link ?>" class="<?= $x==3?'entry-big':'entry' ?>">
                    <?php if($x==3): ?>
                        <div>
                            <img src="<?= base_url('img/blog/'.$d->foto_grande) ?>" alt="<?= $d->titulo ?>">
                        </div>
                    <?php else: ?>
                        <img src="<?= $d->foto ?>" alt="<?= $d->titulo ?>">
                    <?php endif ?>
                    
                    
                    <h4 class="<?= $colores[$x] ?>"><?= $d->categoria ?></h4>
                    <p class="small">Published at <?= strftime('%d de %B',strtotime($d->fecha)) ?> | By: <?= $d->user ?></p>
                    <hr class="green">
                    <h3><?= $d->titulo ?></h3>
                    <p><?= $d->texto ?></p>
                    <p class="link green">Read more</p>
                </a>
            <?php $x++; $x = $x==4?0:$x; endforeach; ?>
        </div>

        <div class="bg-black container-fluid contenedor-mapa-sitio">
            <?php include('mapa-sitio.php');?>
        </div>

        <?php include('librerias.php');?>

    </body>
</html>
