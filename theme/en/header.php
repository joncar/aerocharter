<meta charset="utf-8">
<title>Aerocharter</title>
<meta name="author" content="BluePixel Innovation México">
<meta content="" name="description">
<meta content="BluePixel UX/UI" property="og:title">
<meta content="width=device-width, initial-scale=1" name="viewport">

<meta property="og:image" content="../images/logo_small-p-500.png" />
<meta property="og:description" content="" />
<meta property="og:url"content="http://bluepixel.mx/" />
<meta property="og:title" content="BluePixel UX/UI" />

<meta name="keywords"  content="">

<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,700" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="[base_url]css/styles.css">
<link rel="stylesheet" href="[base_url]css/queries.css">

<!-- Animaciones -->
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<link rel="stylesheet" href="[base_url]css/styles.css" />
<link rel="stylesheet" href="[base_url]css/aos.css" />