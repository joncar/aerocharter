<a class="navbar-brand" href="<?= base_url() ?>index.php">
    <img src="[base_url]images/aerocharter-logo.png" alt="Logo de Aerocharter">
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse flex-row-reverse" id="navbarNav">
    <ul class="navbar-nav">
        <li class="nav-item active">
            <a class="nav-link" href="<?= base_url() ?>index.php#home">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url() ?>index.php#nosotros">About us</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url() ?>index.php#servicios">Services</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="<?= base_url() ?>blog">News</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="<?= base_url() ?>index.php#historia">History</a>
        </li>
        <!--
        <li class="nav-item">
            <a class="nav-link disabled" href="<?= base_url() ?>#equipo">Equipo</a>
        </li>-->
        <li class="nav-item">
            <a class="nav-link disabled" href="<?= base_url() ?>index.php#contacto">Contact</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled btn-cotiza" href="<?= base_url() ?>cotizador.php" style="border-radius: 10px;">Quote now</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="<?= base_url() ?>main/traduccion/es">Spanish</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="<?= base_url() ?>main/traduccion/en">English</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="<?= base_url() ?>#"><i class="fab fa-facebook"></i></a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="<?= base_url() ?>#"><i class="fab fa-twitter"></i></a>
        </li>
        <!--
        <li class="nav-item" style="margin-top: 6px;">
            <a href="#">EN</a> /
            <a href="#">ES</a>
        </li>-->
    </ul>
</div>
