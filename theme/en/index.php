<!DOCTYPE html>
<html lang="es-mx">
    <head>
        <?php include('header.php');?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

    <body>
        <h1>Aerocharter</h1>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php include('menu.php');?>
        </nav>

        <div id="home">
            <h3>AIRPORT SERVICES AS INTEGRAL SUPPORT</h3>
            <h2>TO THE AVIATION AND GROUND HANDLING INDUSTRY</h2>
            <p>45 years offering technical, administrative, operational, and commercial services to the airline industry globally.</p>
            <figure data-aos="fade-up" data-aos-duration="900000">
                <a href="cotizador.php"><img src="[base_url]images/circle_of_service-en.png" alt="Círculo de Servicios Aerocharter" class="slide-fwd-center"></a>
            </figure>
        </div>

        <div id="nosotros">
            <div class="white">
                <h4>GLOBAL LEADER</h4>
                <p>Aerocharter, a Mexican company that for 45 years has offered a broad portfolio of administrative, operational, technical, and commercial services to the airline industry globally, positioning us as the best option for our customers, seeking to provide high-quality and personalized service every day.</p>
                <!--<a href="#">Leer más</a>-->
            </div>
            <div class="blue">
                <h4 data-aos="fade-in" data-aos-duration="8000">MISSION</h4>
                <p>To provide operational, technical, and administrative support to the airline industry through services that add value and meet the expectations of our customers, employees, shareholders, and users.</p>
            </div>
            <div class="blue">
                <h4 data-aos="fade-in" data-aos-duration="8000">VISION</h4>
                <p>To be recognized by the airline industry as the best provider of operational, technical, administrative, and commercial services with a world class level.</p>
            </div>
        </div>

        <div id="servicios">
            <h2>SERVICES</h2>

            <!--
            <ul class="nav nav-tabs">
                <li class="active tab1">Servicio<br>de carga</a></li>
                <li class="tab2"><a data-toggle="tab" href="#menu1">Rep.<br>Legal</a></li>
                <li class="tab3"><a data-toggle="tab" href="#menu2">Servicio<br> a pax.</a></li>
                <li class="tab4"><a data-toggle="tab" href="#menu3">Operación<br> en rampa</a></li>
                <li class="tab5"><a data-toggle="tab" href="#menu4">Venta<br> de GSA</a></li>
            </ul>-->

            <!--
            <div class="tab-content">
                <div id="casa" class="tab-pane active">
                    <h3>HOME</h3>
                    <ul>
                        <li>Agente General de Ventas (GSSA)<br>Capacitado, con experiencia y reconocido.</li>
                        <li>Servicio a Vuelos de Fletamento y Carga</li>
                        <li>Manejo y almacenaje de Carga</li>
                        <li>Supervisión del manejo de carga</li>
                        <li>Manejo de carga en plataforma y almacén</li>
                        <li>Desarrollo de negocios comerciales de carga</li>
                        <li>Servicio al Cliente bilingüe</li>
                        <li>Ventas / Ventas a Consignación</li>
                    </ul>
                </div>
                <div id="menu1" class="tab-pane fade">
                    <h3>Menu 1</h3>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <h3>Menu 2</h3>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                </div>
                <div id="menu3" class="tab-pane fade">
                    <h3>Menu 3</h3>
                    <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                </div>
                <div id="menu4" class="tab-pane fade">
                    <h3>Menu 4</h3>
                    <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                </div>
            </div>-->


            <div>
              <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#1" class="btn-tab-1">CARGO<br>SERVICE</a>
                </li>

                <li>
                    <a data-toggle="tab" href="#2" class="btn-tab-2 hidden-xs">LEGAL<br>REPRESENTATION</a>
                    <a data-toggle="tab" href="#2" class="btn-tab-2 visible-xs">LEGAL<br>REP.</a>
                </li>

                <li>
                    <a data-toggle="tab" href="#3" class="btn-tab-3">PAX<br>SERVICE</a>
                </li>

                <li>
                    <a data-toggle="tab" href="#4" class="btn-tab-4">RAMP<br>OPERATIONS</a>
                </li>

                <li>
                    <a data-toggle="tab" href="#5" class="btn-tab-5">GSA<br>SALE</a>
                </li>
              </ul>

              <div class="tab-content">
                <div id="1" class="row tab-pane fade in active">
                  <div class="col-sm-12">
                      <div class="col-sm-12 text-center titulo-tabs">
                        <b>CARGO SERVICE</b>
                      </div>
                      <div class="col-sm-6">
                          <img src="[base_url]images/tab1.png" class="center-block" alt="Servicios">
                      </div>

                      <div class="col-sm-6">
                          <ul style=" list-style-type: none;">
                            <li><i class="fas fa-plane"></i>  Trained General Sales Agent (GSSA), experienced and recognized.</li>
                            <li><i class="fas fa-plane"></i>  Service to Charter and Freight Flights</li>
                            <li><i class="fas fa-plane"></i>  Cargo handling and storage</li>
                            <li><i class="fas fa-plane"></i>  Cargo management supervision</li>
                            <li><i class="fas fa-plane"></i>  Cargo handling in platform and warehouse</li>
                            <li><i class="fas fa-plane"></i>  Development of commercial cargo businesses</li>
                            <li><i class="fas fa-plane"></i>  Bilingual Customer Service</li>
                            <li><i class="fas fa-plane"></i>  Sales / Consignment sales</li>
                          </ul>
                      </div>
                  </div>
                </div>

                <div id="2" class="row tab-pane fade">
                  <div class="col-sm-12">
                      <div class="col-sm-12 text-center titulo-tabs">
                        <b>LEGAL REPRESENTATION</b>
                      </div>
                      <div class="col-sm-6">
                          <img src="[base_url]images/tab2.png" class="center-block" alt="Servicios">
                      </div>

                      <div class="col-sm-6">
                          Legal Representative before governmental authorities and commercial needs to obtain:
                          <ul style=" list-style-type: none;">
                            <li><i class="fas fa-plane"></i>  Air Operator Certificate (AOC).</li>
                            <li><i class="fas fa-plane"></i>  Permits for chartering and/or regular operations.</li>
                            <li><i class="fas fa-plane"></i>  Slot management.</li>
                            <li><i class="fas fa-plane"></i>  Approach to authorities, hiring of personnel.</li>
                            <li><i class="fas fa-plane"></i>  Special attention to government flights</li>
                            <li><i class="fas fa-plane"></i>  Government affairs</li>
                          </ul>
                      </div>
                  </div>
                </div>

                <div id="3" class="row tab-pane fade">
                  <div class="col-sm-12">
                      <div class="col-sm-12 text-center titulo-tabs">
                        <b>PAX SERVICE</b>
                      </div>
                      <div class="col-sm-6">
                          <img src="[base_url]images/tab3.png" class="center-block" alt="Servicios">
                      </div>

                      <div class="col-sm-6">
                          <ul style=" list-style-type: none;">
                            <li><i class="fas fa-plane"></i>  Sale and reservations</li>
                            <li><i class="fas fa-plane"></i>  Baggage handling</li>
                            <li><i class="fas fa-plane"></i>  Attention at counters</li>
                            <li><i class="fas fa-plane"></i>  Customer service</li>
                            <li><i class="fas fa-plane"></i>  WCH wheelchair assistance</li>
                            <li><i class="fas fa-plane"></i>  Baggage Room</li>
                            <li><i class="fas fa-plane"></i>  Baggage handling in transfer/connection</li>
                          </ul>
                      </div>
                  </div>
                </div>

                <div id="4" class="row tab-pane fade">
                  <div class="col-sm-12">
                      <div class="col-sm-12 text-center titulo-tabs">
                        <b>RAMP OPERATIONS</b>
                      </div>
                      <div class="col-sm-6">
                          <img src="[base_url]images/tab4.png" class="center-block" alt="Servicios">
                      </div>

                      <div class="col-sm-6">
                          <ul style=" list-style-type: none;">
                            <li><i class="fas fa-plane"></i>  Weight and Balance (office)</li>
                            <li><i class="fas fa-plane"></i>  Loading and unloading</li>
                            <li><i class="fas fa-plane"></i>  Cabin Cleaning / Deep Cleaning</li>
                            <li><i class="fas fa-plane"></i>  Moving aircraft (pushback and trailer)</li>
                            <li><i class="fas fa-plane"></i>  Signaling</li>
                            <li><i class="fas fa-plane"></i>  Cargo Insurance and Security</li>
                            <li><i class="fas fa-plane"></i>  Drinking water and sanitary service</li>
                            <li><i class="fas fa-plane"></i>  Power plants (GPU) and starters (ASU)</li>
                            <li><i class="fas fa-plane"></i>  Line maintenance (In process)</li>
                          </ul>
                      </div>
                  </div>
                </div>

                <div id="5" class="row tab-pane fade">
                  <div class="col-sm-12">
                      <div class="col-sm-12 text-center titulo-tabs">
                        <b>GSA SALE</b>
                      </div>
                      <div class="col-sm-6">
                          <img src="[base_url]images/tab5.png" class="center-block" alt="Servicios">
                      </div>

                      <div class="col-sm-6">
                          <ul style=" list-style-type: none;">
                            <li><i class="fas fa-plane"></i>  Sales teams trained, experienced and reliable</li>
                            <li><i class="fas fa-plane"></i>  Customer relationships established for many years</li>
                            <li><i class="fas fa-plane"></i>  Knowledge of the local and global market</li>
                            <li><i class="fas fa-plane"></i>  Development of marketing strategies, advertising and positioning campaigns</li>
                            <li><i class="fas fa-plane"></i>  Accounting and collection through the CASS</li>
                            <li><i class="fas fa-plane"></i>  Deep knowledge of the different types of aircraft and of all types of charges acceptable according to air transport regulations.</li>
                            <li><i class="fas fa-plane"></i>  Regulation of Dangerous Goods.</li>
                            <li><i class="fas fa-plane"></i>  Design, development and creation of new products and logistics solutions</li>
                            <li><i class="fas fa-plane"></i>  Sales and Statistics Reports.</li>
                            <li><i class="fas fa-plane"></i>  Market Analysis and development studies</li>
                          </ul>
                      </div>
                  </div>
                </div>

              </div>
            </div>


        </div>

        <div id="noticias">
            <h2>News</h2>
            <div class="noticias">
                <div class="not1">
                </div>
                <div class="not2">
                    <figure>
                        <img src="[base_url]images/not2.png" alt="Noticia 2">
                    </figure>
                    <div>
                        <h4>Engineers<br>are weird</h4>
                        <p data-aos="fade-up" data-aos-duration="5000">The flight from Apodaca to Mumbai was chartered by DHL Global Forwarding, with the shipment packaged in a 7-metre long box alongside some support equipment required for loading.</p>
                        <a href="entrada.html">Leer más</a>
                    </div>
                </div>
                <div class="not1">
                </div>
            </div>
        </div>

        <div id="historia">
            <h2>Historia</h2>
            <div class="historia">
                <h3>Historia</h3>

                <!--
                <ul>
                    <li class="li1"><b>1973</b><br>Aerocharter fue fundada en 1973 con el propósito de proveer servicios estratégicos a las aerolíneas internacionales que volaban a México. En 1992 la compañía se convirtió en la mas grande de su tipo.</li>
                    <li class="li2"><b>19XX</b><br>Su división de GSSAs representa a más de 10 operadores de carga internacionales.</li>
                    <li class="li3"><b>19XX</b><br>Actualmente Aerocharterestá posicionada como un proveedor de servicios premium, con el objetivo de fortalecer las relaciones de negocios con sus clientes, autoridades y proveedores de servicio a través de liderazgo, trabajo en equipo y un staff experimentado.</li>
                    <li class="li4"><b>19XX</b><br>Con un servicio One-Stop Shop, Aerocharter satisface las necesidades de servicio de sus clientes, proporcionando un servicio personalizado con servicio de alta calidad en el despacho, operaciones terrestres y asuntos legales con la DGAC y Aeropuertos.</li>
                </ul>-->

                <p class="li1 text-center text-history">
                  <b>Aerocharter was founded in 1973 with the purpose of providing strategic services to international airlines flying to Mexico.<br>
                  In 1992 the company became the largest of its kind.<br><br>
                  Its division of GSSAs represents more than 10 international cargo operators.<br>
                  Aerocharter is currently positioned as a premium service provider, with the aim of strengthening business relationships with its customers, authorities, and service providers through leadership, teamwork, and an experienced staff.<br>
                  With a one-stop shop service, Aerocharter meets the needs of its customers, providing a personalized, high quality service in the dispatch, ground operations, and legal issues with the DGAC and Airports.
                </p>
            </div>


            <div class="clientes">

                <div class="excelencia-texto">
                  <p>Our clients</p>
                </div>

                <div>
                    <!--<a href="https://www.alternativeairlines.com/taca-airlines" target="blank"><img src="[base_url]images/tacaperu-logo.png" alt="Logo de Taca Peru"></a>-->
                    <a href="https://www.interjet.com" target="blank"><img src="[base_url]images/interjet-logo.png" alt="Logo de Interjet"></a>
                    <!--<a href="https://www.klm.com/home/mx/en?popup=no&WT.mc_id=c_mx_sea_google_brand_search_null_null&gclid=EAIaIQobChMI2fbFrsf33gIViTxpCh3buQVYEAAYASAAEgJsXfD_BwE&gclsrc=aw.ds" target="blank"><img src="[base_url]images/klm-logo.png" alt="Logo de KLM"></a>-->
                    <a href="http://demoweb.aerocharter.com.mx/web/index.html" target="blank"><img src="[base_url]images/bora-logo.png" alt="Logo de Volga-Dnepr Airlines"></a>
                    <!--<a href="https://www.alternativeairlines.com/taca-airlines" target="blank"><img src="[base_url]images/taca-logo.png" alt="Logo de Taca"></a>-->
                    <!--<a href="https://www.iccs.com.mx/" target="blank"><img src="[base_url]images/iccs-logo.png" alt="Logo de ICCS Mexico and Latin America"></a>-->
                    <!--<a href="https://www.avianca.com/mx/es/?gclid=EAIaIQobChMI9_CN_Mf33gIVAgxpCh0XFgP9EAAYASAAEgJTKfD_BwE&gclsrc=aw.ds" target="blank"><img src="[base_url]images/aviancacargo-logo.png" alt="Logo de Avianca Cargo"></a>-->
                    <!--<a href="https://www.airbridgecargo.com/en" target="blank"><img src="[base_url]images/airbridgecargo-logo.png" alt="Logo de Air Bridge Cargo"></a>-->
                    <a href="http://www.polaraircargo.com/" target="blank"><img src="[base_url]images/polarair-logo.png" alt="Logo de Polar Air Cargo"></a>
                    <a href="https://www.avianca.com/mx/es/?gclid=EAIaIQobChMIvKSfjsv33gIVCzxpCh2MCQJfEAAYASAAEgL0vfD_BwE&gclsrc=aw.ds" target="blank"><img src="[base_url]images/avianca-logo.png" alt="Logo de Avianca"></a>
                    <a href="https://flairair.ca/" target="blank"><img src="[base_url]images/flair-logo.png" alt="Logo de Flair Airlines"></a>
                    <a href="http://www.mexicana.com/" target="blank"><img src="[base_url]images/mexicana-logo.png" alt="Logo de Mexicana"></a>

                    <a href="http://www.dhl.com/en.html" target="blank"><img src="[base_url]images/dhl-logo.png" alt="Logo DHL"></a>
                    <a href="http://www.aerounion.com.mx/" target="blank"><img src="[base_url]images/aerounion-logo.png" alt="Logo Aero Union"></a>
                    <a href="https://www.ups.com/us/en/global.page" target="blank"><img src="[base_url]images/ups-logo.png" alt="Logo UPS"></a>
                    <a href="https://www.hainanairlines.com/MX/GB/Home?gclid=EAIaIQobChMIlu7em8733gIVgbjACh0DsAZNEAAYASAAEgJgDvD_BwE" target="blank"><img src="[base_url]images/hainan-logo.png" alt="Logo Hainan"></a>
                    <a href="https://www.nationaljets.com/" target="blank"><img src="[base_url]images/national-logo.png" alt="Logo National Jets"></a>
                    <a href="https://www.china-airlines.com/us/en" target="blank"><img src="[base_url]images/china-logo.png" alt="Logo China Airlines"></a>
                    <a href="https://www.estafeta.com/" target="blank"><img src="[base_url]images/estafeta-logo.png" alt="Logo Estafeta"></a>
                    <a href="https://www.cargologicair.com/" target="blank"><img src="[base_url]images/cargo-logic-logo.png" alt="Logo Cargo Logic Air"></a>
                    <a href="http://www.aeronavestsm.com/" target="blank"><img src="[base_url]images/tsm-logo.png" alt="Logo Cargo Logic Air"></a>
                    <a href="https://global.csair.com/" target="blank"><img src="[base_url]images/china-southern-logo.png" alt="Logo China Southern"></a>
                    <a href="http://www.atlasair.com/holdings/index.asp" target="blank"><img src="[base_url]images/atlas-logo.png" alt="Logo Atlas Air"></a>
                    <a href="http://www.aviancacargo.com/index.aspx" target="blank"><img src="[base_url]images/tampa-logo.png" alt="Logo Tampa cargo"></a>
                    <a href="http://www.atlanta.is/" target="blank"><img src="[base_url]images/atlanta-logo.png" alt="Logo Atlanta"></a>
                    <a href="https://www.afklcargo.com/WW/en/local/homepage/homepage.jsp" target="blank"><img src="[base_url]images/airfrance-logo.png" alt="Logo Air France Cargo"></a>
                    <a href="https://www.volaris.com/?gclid=EAIaIQobChMIn52F8dP33gIVgh5pCh1NBQU9EAAYASAAEgJczfD_BwE" target="blank"><img src="[base_url]images/volaris-logo.png" alt="Logo Volaris"></a>
                    <a href="http://www.kalittaair.com/" target="blank"><img src="[base_url]images/kalitta-logo.png" alt="Logo Kalitta Air"></a>
                </div>
            </div>

            <div class="lista">
                <h3>45 YEARS OF EXPERIENCE</h3>
                <ul>
                    <li><i class="fas fa-plane"></i> Founders of the first 100% Mexican airline dedicated to domestic and international cargo and operating a low-cost model with a performance of 98.9% of OTP (OnTime Performance).</li>
                    <li><i class="fas fa-plane"></i> Widely recognized in the national and international airline industry.</li>
                    <li><i class="fas fa-plane"></i> Pioneers in national and international charter service.</li>
                    <li><i class="fas fa-plane"></i> Financial strength to start new business.</li>
                    <li><i class="fas fa-plane"></i> High level of industry and government relations.</li>
                    <li><i class="fas fa-plane"></i> Highly qualified management team.</li>
                </ul>
                <figure>
                    <img src="[base_url]images/cockpit.png" alt="Aerocharter cockpit">
                </figure>
            </div>
        </div>

       <div id="equipo" class="hidden-xs">
            <h2>Equipo</h2>
            <div>
                <div id="uno" data-aos="fade-up" data-aos-duration="9000">
                    <img src="[base_url]images/vector-avion/Ilustracion-fondo.png" alt="Equipo de Aerocharter">
                </div>

                <div id="dos" data-aos="slide-right" data-aos-duration="3000" data-aos-easing="ease-in-quad">
                    <img src="[base_url]images/vector-avion/Ilustracion-01.png" alt="Equipo de Aerocharter">
                </div>

                <div id="tres" data-aos="fade-up" data-aos-duration="3000" data-aos-easing="ease-in-quad">
                    <img src="[base_url]images/vector-avion/Ilustracion-02-en.png" alt="Equipo de Aerocharter">
                </div>
            </div>
        </div>

        <!-- movil -->
        <div class="visible-xs">
            <div data-aos="fade-up" data-aos-duration="9000">
                <img src="[base_url]images/vector-avion/Ilustracion-01-movil.png" alt="Equipo de Aerocharter">
            </div>
        </div>

        <div id="contacto">
            <?php include('footer.php');?>
        </div>

        <div class="bg-black container-fluid contenedor-mapa-sitio">
            <?php include('mapa-sitio.php');?>
        </div>

        <?php include('librerias.php');?>

    </body>
</html>
