<!DOCTYPE html>
<html lang="es-mx">
    <head>
        <?php include('header.php');?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

    <body>
        <h1>Aerocharter</h1>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php include('menu-interno.php');?>
        </nav>

        <div class="fondo-entrada">
            <div class="entrada">
                <h3><?= $detail->categoria ?></h3>
                <p class="fecha">Published at <?= strftime('%d of %B',strtotime($detail->fecha)); ?> | By: <a href="#"><?= $detail->user ?></a></p>
                <h2><?= $detail->titulo ?></h2>
                <img src="<?= $detail->foto ?>"><br><br>
                <p><?= strip_tags($detail->texto) ?></p>

                <a href="<?= base_url('blog') ?>"><button class="btn btn-footer"><b>Back to blog</b></button></a>
            </div>
            <div class="sidebar">
                <h5>More news</h5>
                <?php $colores = array('green','red','blue2','yellow','pink'); ?>
                <?php foreach($relacionados->result() as $n=>$r): ?>
                    <a href="<?= $r->link ?>" class="entry">
                        <img src="<?= $r->foto ?>">
                        <h4 class="<?= $colores[$n] ?>"><?= $r->titulo ?></h4>
                        <p class="small">Published at <?= strftime('%d of %B',strtotime($r->fecha)); ?> | By: <?= $r->user ?></p>
                        <hr class="<?= $colores[$n] ?>">
                        <h3><?= $r->titulo ?></h3>
                    </a>
                <?php endforeach ?>                
            </div>
        </div>

        <div class="bg-black container-fluid contenedor-mapa-sitio">
            <?php include('mapa-sitio.php');?>
        </div>

        <?php include('librerias.php');?>

    </body>
</html>
