<!DOCTYPE html>
<html lang="es-mx">
    <head>
        <?php include('header.php');?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

    <body>
        <h1>Aerocharter</h1>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php include('menu-interno.php');?>
        </nav>

        <div class="header2">
            <h2>Quoting</h2>
        </div>

        <div class="process">
            <div class="card1">
                <h2>Quote</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis augue sollicitudin facilisis sollicitudin. Phasellus eleifend convallis ligula.</p>
            </div>
            <div class="card2">
                <h2>Request</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis augue sollicitudin facilisis sollicitudin. Phasellus eleifend convallis ligula.</p>
            </div>
            <div class="card3">
                <h2>Employ</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis augue sollicitudin facilisis sollicitudin. Phasellus eleifend convallis ligula.</p>
            </div>
        </div>

        <div class="cotizador" style="margin-bottom: 100px;">
            <h2>Your information</h2>
            <form class="form-contacto form-horizontal" method="post" action="cotizador-envio.php">
                <div>
                    <div class="input">
                        <h3>Name of the airline</h3>
                        <input type="text" name="nombreaerolinea" id="nombreaerolinea" placeholder="...">
                    </div>
                    <div class="input">
                        <h3>Airports where it operates</h3>
                        <input type="text" name="nombreoperacion" id="nombreoperacion" placeholder="...">
                    </div>
                    <div class="input">
                        <h3>Approximate number of monthly operations</h3>
                        <input type="text" name="operacionesmensuales" id="operacionesmensuales" placeholder="...">
                    </div>
                </div>

                <div>
                    <!--
                    <div class="input input-cotiza">
                        <h3>Tipo de Aerolínea</h3>
                        <fieldset>
                            <label>
                              <input type="radio" name="test" value="small" checked>
                              <img src="[base_url]images/select-01.png"> a.Pasajero
                            </label>

                            <label>
                              <input type="radio" name="test" value="big">
                              <img src="[base_url]images/select-02.png"> b.Carga
                            </label>
                        </fieldset>
                    </div>

                    <div class="input input-cotiza">
                        <h3>Tipos de Vuelos</h3>
                        <fieldset>
                            <label>
                              <input type="radio" name="test" value="small">
                              <img src="[base_url]images/select-01.png"> a.Vivo/Vivo
                            </label>

                            <label>
                              <input type="radio" name="test" value="big">
                              <img src="[base_url]images/select-02.png">  b. Vacío/Vivo
                            </label>

                            <label>
                              <input type="radio" name="test" value="big">
                              <img src="[base_url]images/select-02.png">  c. Vivo/Vacío
                            </label>
                        </fieldset>
                    </div>-->
                    

                    <div class="input input-cotiza">
                        <h3>Type of Airline</h3><br>
                        <select class="form-control" id="selectaerolinea" name="selectaerolinea">
                          <option disabled="seleccionar">Select</option>
                          <option value="Pasajero">a. Passenger</option>
                          <option value="Carga">b. Load</option>
                        </select>
                    </div>

                    <div class="input input-cotiza">
                        <h3>Type of Flight</h3><br>
                        <select class="form-control" id="selectvuelo" name="selectvuelo">
                          <option disabled="seleccionar">Select</option>
                          <option value="Vivo/Vivo">a. Vivo/Vivo</option>
                          <option value="Vacío/Vivo">b. Empty/Vivo</option>
                          <option value="Vivo/Vacío">c. Vivo/Empty</option>
                        </select>
                    </div>

                    <div class="input input2">
                        <h3>Contact information</h3>
                        <input type="text" name="nombre" id="nombre" placeholder="Name...">
                        <input type="text" name="puesto" id="puesto" placeholder="Position in the company...">
                        <input type="text" name="ciudad" id="ciudad" placeholder="Location city...">
                        <input type="text" name="telefono" id="telefono" placeholder="Phone...">
                        <input type="email" name="correo" id="correo" placeholder="Email...">
                    </div>


                </div>
                <input type="submit" value="Send">
            </form>
        </div>

        <div class="bg-black container-fluid contenedor-mapa-sitio">
            <?php include('mapa-sitio.php');?>
        </div>

        <?php include('librerias.php');?>

    </body>
</html>
