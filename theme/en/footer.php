<div>
    <h2>Contact us</h2>
    <a href="mailto:contacto@aerocharter.mx"><i class="fa fa-envelope"></i> contacto@aerocharter.mx</a>
    <a href="tel:+52155933040"><i class="fa fa-phone"></i> 1(+52)55933040</a>
    <i class="fas fa-map-marker-alt"></i> Av. Paseo de las Palmas #215, Oficina 503 Col. Lomas de Chapultepec V Sección, Del. Miguel Hidalgo, C.P. 11000, CDMX.
</div>
<div class="blue-bg text-center">
    <figure>
        <h4>About us</h4>
        <p>
            45 years offering technical, administrative, operational, and commercial services to the airline industry globally.
            <a href="<?= base_url() ?>cotizador.php"><button class="btn btn-footer"><b>¡Quote!</b></button></a>
        </p>
    </figure>
</div>
<div class="formulario">
    <form class="form-contacto form-horizontal" method="post" action="contacto-envio.php">
        <input type="text" name="nombre" id="nombre" placeholder="Name..." required>
        <input type="email" name="correo" id="correo" placeholder="Email..." required>
        <textarea rows="4" required placeholder="Message..."></textarea>
        <input type="submit" value="Send">
    </form>
</div>
